import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

 class PerimeterRectangleTest {

    @Test
    void Testcase1(){
        PerimeterRectangle perimeterRectangle= new PerimeterRectangle(3.3,4.5);
        Double ans= perimeterRectangle.perimeter();
        assertEquals(15.6,ans);
    }
     @Test
     void Testcase2(){
         PerimeterRectangle perimeterRectangle= new PerimeterRectangle(5.5,6.5);
         Double ans= perimeterRectangle.perimeter();
         assertEquals(24,ans);
     }
}
