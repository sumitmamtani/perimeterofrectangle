 class PerimeterRectangle {
    private Double length;
    private  Double width;

     PerimeterRectangle(Double length, Double width) {
        this.length = length;
        this.width = width;
    }
     Double perimeter(){

        return (2*(length + width));
    }
}
